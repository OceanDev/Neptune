# Neptune

A bot that can draw images on the canvas utilizing multiple accounts for r/place v2.
I am not actually certain if it works.

# Installation

- Install [Poetry](https://python-poetry.org/)
- Clone the Neptune repository
- run `poetry install`

# Usage

Create a file with a list of `reddit_session` cookies on each line.

Example: `poetry run python placebot config/snek.png -s sessions.txt -x 727 -y 727 --width 30 --brightness 1.5`

The `x` and `y` coordinates are at the top left of the image.

# Note

Using this with more than 2 accounts will probably get them ratelimited and unable to place any more pixels.
