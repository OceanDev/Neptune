from io import BytesIO
import random
from websockets.legacy.client import WebSocketClientProtocol
from websockets.typing import Origin, Subprotocol
from PIL import Image, ImageEnhance
import websockets.exceptions
from threading import Thread
from base64 import b64decode
from typing import Optional
import websockets.client
from math import sqrt
import numpy as np
import colorsys
import asyncio
import aiohttp
import time
import json


class Palette:
	colors: list[Optional[int]]
	color_rgb_tuples: list[Optional[tuple[int, int, int]]]
	
	def __init__(self, colors: list[Optional[int]]):
		self.colors = colors
		self.color_rgb_tuples = list(map(lambda c: (c >> 16 & 0xFF, c >> 8 & 0xFF, c & 0xFF) if c is not None else None, self.colors))
	
	@staticmethod
	def from_index_hex_dict_list(index_to_hex_dict: list):
		colors_to_indexes = {}
		highest_index = 0
		for item in index_to_hex_dict:
			colors_to_indexes[item['index']] = int(item['hex'][1:], 16)
			if item['index'] > highest_index:
				highest_index = item['index']
		
		colors = [None] * (highest_index + 1)
		for index, color in colors_to_indexes.items():
			colors[index] = color
		
		print('colors', colors)
		return Palette(colors)




	def color_from_rgb(self, rgb: tuple[int, int, int]):
		# hsv = colorsys.rgb_to_hsv(rgb[0] / 255, rgb[1] / 255, rgb[2] / 255)
		# palette_hsv = [colorsys.rgb_to_hsv((c >> 16 & 0xFF) / 255, (c >> 8 & 0xFF) / 255, (c & 0xFF) / 255) for c in self.colors]
		# # find closest color in the palette
		# closest_color_index = None
		# closest_color_distance = float('inf')
		# for i, palette_color in enumerate(palette_hsv):
		# 	distance = abs(palette_color[0] - hsv[0]) * .475 + abs(palette_color[1] - hsv[1]) * .2875 + abs(palette_color[2] - hsv[2]) * .2375
		# 	if closest_color_index is None or distance < closest_color_distance:
		# 		closest_color_index = i
		# 		closest_color_distance = distance

		# assert closest_color_index is not None
		# return Pixel(closest_color_index)

		if rgb in self.color_rgb_tuples:
			return Pixel(self.color_rgb_tuples.index(rgb))
		
		smallest_distance_index = -1
		smallest_distance = float('inf')
		for i, color in enumerate(self.color_rgb_tuples):
			if color is None:
				continue
			
			distance = sqrt((color[0] - rgb[0]) ** 2 + (color[1] - rgb[1]) ** 2 + (color[2] - rgb[2]) ** 2)
			if smallest_distance_index < 0 or distance < smallest_distance:
				smallest_distance_index = i
				smallest_distance = distance

		# color_rgb_tuples_without_none = list(filter(lambda c: c is not None, self.color_rgb_tuples))
		# colors = np.array(color_rgb_tuples_without_none)
		# color = np.array(rgb)

		# # python typings don't like us subtracting arrays, but it works
		# distances = np.sqrt(np.sum((colors-color)**2,axis=1))  # type: ignore

		# index_of_smallest = np.where(distances==np.amin(distances))
		# smallest_distance = tuple(colors[index_of_smallest][0])

		return Pixel(smallest_distance_index)

class CanvasConfig:
	dx: int
	dy: int
	index: int

	def __init__(self, dx: int, dy: int, index: int):
		self.dx = dx
		self.dy = dy
		self.index = index


class Config:
	width: int
	height: int
	canvas_configurations: list[CanvasConfig]
	palette: Palette
	individual_canvas_width: int
	individual_canvas_height: int

	def __init__(self, individual_canvas_width: int, individual_canvas_height: int, width: int, height: int, palette: Palette, canvas_configurations: list[CanvasConfig]):
		self.individual_canvas_width = individual_canvas_width
		self.individual_canvas_height = individual_canvas_height
		self.width = width
		self.height = height
		self.palette = palette
		self.canvas_configurations = canvas_configurations
	

	def determine_canvas_id_and_new_coords(self, x: int, y: int):
		for canvas_config in self.canvas_configurations:
			if x >= canvas_config.dx and x < canvas_config.dx + self.individual_canvas_width and y >= canvas_config.dy and y < canvas_config.dy + self.individual_canvas_height:
				return canvas_config.index, x - canvas_config.dx, y - canvas_config.dy
		
		raise Exception('Could not determine canvas id')

class Pixel:
	color_index: int

	def __init__(self, color_index: int):
		self.color_index = color_index
	
	def __eq__(self, other):
		return self.color_index == other.color_index
	
	def copy(self):
		return Pixel(self.color_index)

class Canvas:
	pixels: list[Pixel]
	config: Config

	def __init__(self, config: Config):
		self.pixels = [Pixel(0) for _ in range(config.width * config.height)]
		self.config = config
	
	def get_pixel(self, x: int, y: int) -> Pixel:
		return self.pixels[y * self.config.width + x]
	
	def set_pixel(self, x: int, y: int, pixel: Pixel):
		self.pixels[y * self.config.width + x] = pixel
		
	def render(self, filename):
		print('rendering', self.config.width, self.config.height)
		canvas_im = Image.new('RGB', (self.config.width, self.config.height))
		datas = []
		for i, pixel in enumerate(self.pixels):
			pixel_coords_x, pixel_coords_y = i % canvas_im.width, i // canvas_im.width
			color = self.config.palette.colors[pixel.color_index]
			if color is None:
				raise Exception(f'Trying to render a pixel with no color ({pixel.color_index} at {pixel_coords_x}, {pixel_coords_y})')
			datas.append((
				color >> 16 & 0xFF,
				color >> 8 & 0xFF,
				color & 0xFF
			))
		canvas_im.putdata(datas)
		canvas_im.save(filename)
	
	def overlay_data(self, data, diff_png_url: str, canvas_id: int):
		canvas_config = None
		for canvas_config in self.config.canvas_configurations:
			if canvas_config.index == canvas_id:
				break
		if not canvas_config:
			raise Exception(f'Could not find canvas with id {canvas_id}')
		dx, dy = canvas_config.dx, canvas_config.dy

		try:
			diff_im = Image.open(BytesIO(data))
		except Exception as e:
			print('error parsing image', e, diff_png_url)
			return

		diff_im = diff_im.convert('RGBA')
		# iterate over the pixels and apply the non-transparent ones to our canvas
		rgb_color_tuples = self.config.palette.color_rgb_tuples

		np_im = np.array(diff_im)
		y_list, x_list = np.where(np.all(np_im != (0, 0, 0, 0), axis=2))

		# at this point it's faster to not use numpy
		if len(x_list) >= 1000:
			for i, rgba in enumerate(diff_im.getdata()):
				if rgba[3] == 255:
					# color_pixel = self.color_from_rgb((r, g, b))
					color_pixel = Pixel(rgb_color_tuples.index((rgba[0], rgba[1], rgba[2])))
					self.set_pixel(dx + (i % diff_im.width), dy + i // diff_im.width, color_pixel)
		else:
			for (x, y) in np.column_stack((x_list, y_list)):
				rgba = np_im[y, x]
				if rgba[3] == 255:
					# color_pixel = self.color_from_rgb((r, g, b))
					color_pixel = Pixel(rgb_color_tuples.index((rgba[0], rgba[1], rgba[2])))
					self.set_pixel(dx + x, dy + y, color_pixel)
	
	def copy(self):
		canvas = Canvas(self.config)
		for i, pixel in enumerate(self.pixels):
			canvas.pixels[i] = pixel.copy()
		return canvas

	

class Client:
	ws: WebSocketClientProtocol
	session: aiohttp.ClientSession
	canvas: Optional[Canvas]
	next_available_pixel_seconds_since_epoch: int
	session_token_part: str
	session_value: str
	save_canvas: bool

	async def init(self, session: str, save_canvas: bool=True):
		self.session = aiohttp.ClientSession()
		r = await self.session.get('https://hot-potato.reddit.com/embed', headers={
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
			'Cookie': f'reddit_session={session}'
		})
		token_v2_cookie = r.cookies.get('token_v2')
		if not token_v2_cookie:
			raise Exception('No token_v2 cookie')
		self.token_v2_cookie_value = token_v2_cookie.value
		session_token_part = json.loads(b64decode(self.token_v2_cookie_value.split('.')[1] + '==='))['sub']
		self.session_token_part = session_token_part
		self.session_value = session
		self.save_canvas = save_canvas

		r = await self.session.post('https://gql-realtime-2.reddit.com/query', json={
			"operationName":"getUserCooldown","variables":{"input":{"actionName":"r/replace:get_user_cooldown"}},"query":"mutation getUserCooldown($input: ActInput!) {\n  act(input: $input) {\n    data {\n      ... on BasicMessage {\n        id\n        data {\n          ... on GetUserCooldownResponseMessageData {\n            nextAvailablePixelTimestamp\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"
		}, headers={
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
			'Authorization': f'Bearer {self.session_token_part}',
			'Origin': 'https://hot-potato.reddit.com',
			'Referer': 'https://hot-potato.reddit.com/'
		})
		cooldown_data = await r.json()
		try:
			next_pixel_timestamp_response = cooldown_data['data']['act']['data'][0]['data']['nextAvailablePixelTimestamp']
		except Exception as e:
			print('Error parsing cooldown data?')
			print(cooldown_data)
			raise e
		self.next_available_pixel_seconds_since_epoch = (next_pixel_timestamp_response / 1000 + random.randint(1, 10)) if next_pixel_timestamp_response else 0

		await self._connect_to_websocket()
		
		print('gotten stuff :)')
	
	async def _connect_to_websocket(self):
		self.ws = await websockets.client.connect('wss://gql-realtime-2.reddit.com/query', extra_headers={
			'Host': 'gql-realtime-2.reddit.com',
			'Cookie': f'token_v2={self.session_value}',
			# 'Sec-WebSocket-Protocol': 'graphql-ws',
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0'
		}, origin=Origin('https://hot-potato.reddit.com'), subprotocols=[Subprotocol('graphql-ws')])

		await self.ws.send(f'{{"type":"connection_init","payload":{{"Authorization":"Bearer {self.session_token_part}"}}}}')
		request_id = 1
		await self.ws.send(json.dumps({"id": str(request_id),"type":"start","payload":{"variables":{"input":{"channel":{"teamOwner":"AFD2022","category":"CONFIG"}}},"extensions":{},"operationName":"configuration","query":"subscription configuration($input: SubscribeInput!) {\n  subscribe(input: $input) {\n    id\n    ... on BasicMessage {\n      data {\n        __typename\n        ... on ConfigurationMessageData {\n          colorPalette {\n            colors {\n              hex\n              index\n              __typename\n            }\n            __typename\n          }\n          canvasConfigurations {\n            index\n            dx\n            dy\n            __typename\n          }\n          canvasWidth\n          canvasHeight\n          __typename\n        }\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"}}))
		request_id += 1

		# subscribe to config update and wait for it and then return
		# 
		connection_ack_packet = await self._recv_packet()
		assert connection_ack_packet['type'] == 'connection_ack'
		ka_packet = await self._recv_packet()
		assert ka_packet['type'] == 'ka'
		print('Authenticated')
		config_data_packet = await self._recv_packet()
		config_data = config_data_packet['payload']['data']['subscribe']['data']
		palette_dict_list = config_data['colorPalette']['colors']
	

		print('config_data_packet', config_data_packet)

		canvas_configs: list[CanvasConfig] = []
		for canvas_config in config_data['canvasConfigurations']:
			canvas_configs.append(CanvasConfig(canvas_config['dx'], canvas_config['dy'], canvas_config['index']))
		
		if self.save_canvas:
			for canvas_config in canvas_configs:
				print('canvas_config.index', canvas_config.index)
				await self.ws.send(json.dumps({"id": str(request_id),"type":"start","payload":{"variables":{"input":{"channel":{"teamOwner":"AFD2022","category":"CANVAS","tag": str(canvas_config.index)}}},"extensions":{},"operationName":"replace","query":"subscription replace($input: SubscribeInput!) {\n  subscribe(input: $input) {\n    id\n    ... on BasicMessage {\n      data {\n        __typename\n        ... on FullFrameMessageData {\n          __typename\n          name\n          timestamp\n        }\n        ... on DiffFrameMessageData {\n          __typename\n          name\n          currentTimestamp\n          previousTimestamp\n        }\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"}}))
				request_id += 1
	


		individual_canvas_width = config_data['canvasWidth']
		individual_canvas_height = config_data['canvasHeight']
		width = 0
		height = 0
		for canvas_config in canvas_configs:
			if canvas_config.dx + individual_canvas_width > width:
				width = canvas_config.dx + individual_canvas_width
			if canvas_config.dy + individual_canvas_height > height:
				height = canvas_config.dy + individual_canvas_height
		config = Config(
			individual_canvas_width=individual_canvas_width,
			individual_canvas_height=individual_canvas_height,
			width=width,
			height=height,
			palette=Palette.from_index_hex_dict_list(palette_dict_list),
			canvas_configurations=canvas_configs
		)
		if self.save_canvas:
			self.canvas = Canvas(config)
		else:
			self.canvas = None


	async def put_pixel(self, x: int, y: int, pixel: Pixel, config: Config) -> bool:
		await self.wait_until_available()
		canvas_id, canvas_x, canvas_y = config.determine_canvas_id_and_new_coords(x, y)
		r = await self.session.post(
			'https://gql-realtime-2.reddit.com/query',
			json={"operationName":"setPixel","variables":{"input":{"actionName":"r/replace:set_pixel","PixelMessageData":{"coordinate":{"x": canvas_x,"y": canvas_y},"colorIndex":pixel.color_index,"canvasIndex": canvas_id}}},"query":"mutation setPixel($input: ActInput!) {\n  act(input: $input) {\n    data {\n      ... on BasicMessage {\n        id\n        data {\n          ... on GetUserCooldownResponseMessageData {\n            nextAvailablePixelTimestamp\n            __typename\n          }\n          ... on SetPixelResponseMessageData {\n            timestamp\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"},
			headers={
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
				'Authorization': f'Bearer {self.session_token_part}',
				'Origin': 'https://hot-potato.reddit.com',
				'Referer': 'https://hot-potato.reddit.com/'
			}
		)
		data = await r.json()
		print(data)
		if 'errors' in data:
			if data['errors'][0]['message'] == 'Ratelimited':
				self.next_available_pixel_seconds_since_epoch = data['errors'][0]['extensions']['nextAvailablePixelTs'] / 1000
				print('An account was ratelimited! It can place a pixel in', self.next_available_pixel_seconds_since_epoch - time.time(), 'seconds')
			return False
		else:
			self.next_available_pixel_seconds_since_epoch = data['data']['act']['data'][0]['data']['nextAvailablePixelTimestamp'] / 1000
			return True

	def get_pixel(self, x: int, y: int) -> Pixel:
		if self.canvas:
			return self.canvas.get_pixel(x, y)
		else:
			raise Exception('Canvas not initialized')

	def color_from_rgb(self, rgb: tuple[int, int, int]):
		if self.canvas:
			return self.canvas.config.palette.color_from_rgb(rgb)
		else:
			raise Exception('Canvas not initialized')
	
	def seconds_until_available(self) -> float:
		return self.next_available_pixel_seconds_since_epoch - time.time()

	async def wait_until_available(self):
		print('Waiting', self.seconds_until_available(), 'seconds until next pixel.')
		if self.seconds_until_available() > 0:
			await asyncio.sleep(self.seconds_until_available())
		
	async def _recv_packet(self) -> dict:
		try:
			data = await self.ws.recv()
		except websockets.exceptions.ConnectionClosedError:
			print('Disconnected from websocket! Waiting 15 seconds and reconnecting.')
			await asyncio.sleep(15)
			print('Reconnecting to websocket!')
			await self._connect_to_websocket()
			print('Reconnected!')
			return await self._recv_packet()
		return json.loads(data)
	
	async def _process_packet(self, packet):
		if self.canvas:
			if packet['type'] == 'data':
				# it's a canvas diff update diff packet
				try:
					diff_png_url = packet['payload']['data']['subscribe']['data']['name']
				except:
					print('Error getting url?', packet)
					return
				canvas_id = int(diff_png_url.split('/')[-1].split('.')[0].split('-')[1])

				# get the url and apply the diff to our canvas with pillow!
				r = await self.session.get(diff_png_url)
				diff_png = await r.read()
				self.canvas.overlay_data(diff_png, diff_png_url, canvas_id)
				# print('Rendering canvas!')
				# self.canvas.render()
				# print('Rendered canvas!')

	
	async def run(self):
		while True:
			packet = await self._recv_packet()
			asyncio.create_task(self._process_packet(packet))



