from PIL import Image, ImageEnhance
import hitherdither
import argparse
import asyncio
import place

parser = argparse.ArgumentParser(description='Reddit April Event 2022.')
parser.add_argument('image_file', nargs='?', type=argparse.FileType('rb'), default='config/image.png')
parser.add_argument('--sessions_file', '-s', nargs='?', type=argparse.FileType('rb'), default='config/sessions.txt')
parser.add_argument('--brightness', type=float, default=1)
parser.add_argument('--saturation', type=float, default=1)
parser.add_argument('--width', '-w', type=int, default=None)
parser.add_argument('--dither', '-d', type=bool, default=False, action=argparse.BooleanOptionalAction)
parser.add_argument('--render', '-r', type=bool, default=False, action=argparse.BooleanOptionalAction)
parser.add_argument('-x', type=int)
parser.add_argument('-y', type=int)

args = parser.parse_args()

with args.sessions_file as f:
	sessions: list[str] = list(map(lambda l: l.split()[0].decode(), filter(lambda l: l and not l.startswith(b'#'), f.read().strip().splitlines())))

im = Image.open(args.image_file)

brightness = args.brightness
saturation = args.saturation
width = args.width

if width:
	# resize to the given width, preserving aspect ratio
	im = im.resize((width, int(im.height * width / im.width)), Image.NEAREST)

im = im.convert('RGBA')
# brighten the image
im = ImageEnhance.Brightness(im).enhance(brightness)
# saturate the image
im = ImageEnhance.Color(im).enhance(saturation)
undithered_im = im.copy()

im = im.convert('RGB')

if args.render:
	im.save('imtransformed.png')



def get_next_client(games: list[place.Client]):
	return min(games, key=lambda g: g.next_available_pixel_seconds_since_epoch)

async def main():
	global im

	top_left_coords_x = args.x
	top_left_coords_y = args.y

	print('Logging in...')
	clients: list[place.Client] = []
	for session_index, session in enumerate(sessions):
		client = place.Client()
		# we only save the canvas for the first client, to save memory
		await client.init(session, save_canvas=session_index==0)
		clients.append(client)
	print('All clients ready.')
	assert clients[0].canvas

	asyncio.create_task(clients[0].run())

	# we wait 10 seconds to make sure the canvas is fully downloaded and stuff
	await asyncio.sleep(10)

		
	if args.dither:
		im = hitherdither.ordered.bayer.bayer_dithering(
			im,
			hitherdither.palette.Palette(
				list(filter(
					lambda c: c is not None,
					clients[0].canvas.config.palette.colors
				)),
			),
			[256/4, 256/4, 256/4],
			order=2
		)
		im = im.convert('RGB')

	if args.render:
		print('ok making render')
		# create a preview
		clients[0].canvas.render('canvas.png')
		canvas_copy = clients[0].canvas.copy()
		for i, ((r, g, b), (_, _, _, a)) in enumerate(zip(im.getdata(), undithered_im.getdata())):
			pixel_coords_x, pixel_coords_y = i % im.width, i // im.width
			game_coords_x, game_coords_y = top_left_coords_x + pixel_coords_x, top_left_coords_y + pixel_coords_y
			new_color = canvas_copy.config.palette.color_from_rgb((r, g, b))
			if a < 128: continue
			canvas_copy.set_pixel(game_coords_x, game_coords_y, new_color)
		canvas_copy.render('preview.png')
		print('Made preview')




	while True:
		for i, ((r, g, b), (_, _, _, a)) in enumerate(zip(im.getdata(), undithered_im.getdata())):
			pixel_coords_x, pixel_coords_y = i % im.width, i // im.width

			game_coords_x, game_coords_y = top_left_coords_x + pixel_coords_x, top_left_coords_y + pixel_coords_y
			new_color = clients[0].color_from_rgb((r, g, b))

			# mostly transparent pixels are ignored
			if a < 128: continue

			next_client = get_next_client(clients)
			if next_client.seconds_until_available() > 2:
				await next_client.wait_until_available()
					
			existing_color = clients[0].get_pixel(game_coords_x, game_coords_y)

			if existing_color != new_color:
				print('Putting pixel', new_color.color_index, 'at', game_coords_x, game_coords_y, 'replacing', existing_color.color_index)
				await next_client.put_pixel(game_coords_x, game_coords_y, new_color, clients[0].canvas.config)
				print('Placed', new_color.color_index, 'at', game_coords_x, game_coords_y)
	


asyncio.run(main())


